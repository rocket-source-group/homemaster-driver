//
// Created by Yun Zeng on 2018/4/14.
// 光线传感器
//

#ifndef PLATFORM_LIGHT_H
#define PLATFORM_LIGHT_H

#include <linux/types.h>

#pragma pack(push)
#pragma pack(1)

typedef struct {
    uint32_t lux;
    uint16_t infrared;
}platform_light_data_typedef;

#pragma pack(pop)

typedef int (*light_recv_callback)(platform_light_data_typedef *data);
int platform_light_init(light_recv_callback callback);
int platform_light_exit(void);

int32_t platform_light_write(uint8_t *data, uint16_t len);



#endif // PLATFORM_LIGHT_H
