//
// Created by Yun Zeng on 2018/4/15.
//

#ifndef PLATFORM_OLED_H
#define PLATFORM_OLED_H

#include <linux/types.h>

int platform_oled_init(void);
int platform_oled_exit(void);

int platform_oled_write(uint8_t *buffer, uint16_t len);

#endif // PLATFORM_OLED_H
