//
// Created by Yun Zeng on 2018/4/12.
// HomeMaster 工程驱动程序
// OLED 433(CC1101) INFRARED
//

#ifndef MASTER_H
#define MASTER_H

#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/module.h>

// 在CLion里面编辑时引入的头文件，避免IDE报各种错误
#ifdef EDIT_KERNEL
#include <stdint.h>
#include <string.h>
#include <libubox/utils.h>
#include <asm-generic/uaccess.h>
#include <asm-generic/io.h>
#include <errno.h>
#include <stdint.h>
#endif

#define     MASTER_DEV_NAME     "master"

#define     MASTER_DEV_MAJOR    99
#define     MASTER_DEV_MINOR    1

typedef struct {
    struct cdev     cdev;
    dev_t           devno;
    struct class    *pclass;
}master_dev_typedef;


#endif // MASTER_H
