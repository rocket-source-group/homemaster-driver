//
// Created by Yun Zeng on 2018/4/17.
// 红外遥控发射
//

#ifndef PLATFORM_INFRARED_H
#define PLATFORM_INFRARED_H

#include "master.h"

int platform_infrared_init(master_dev_typedef *dev);
int platform_infrared_exit(master_dev_typedef *dev);

int32_t platform_infrared_write(uint8_t *data, uint16_t len);

#endif // PLATFORM_INFRARED_H
